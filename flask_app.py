from flask import jsonify
from flask import flash, request
from config import app, mqtt, connect_db
import datetime
import psycopg2



def flask_app():
    @app.route('/')
    def welcome():
        return('Welcome')

    @app.route('/api/sensors-now')
    def sensors_now():
        try:
            [db, cursor] = connect_db()
            cursor.execute("SELECT * FROM sensors_now_0 ORDER BY id")
            columns = cursor.description 
            rows = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]
            resp = jsonify(rows)
            resp.status_code = 200
            return resp
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            db.close()
    
    @app.route('/api/sensors-statistic-0')
    def sensors_statistic_0():
        try:
            [db, cursor] = connect_db()
            cursor.execute("SELECT * FROM sensors_statistic_0 ORDER BY time")
            columns = cursor.description 
            rows = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]
            resp = jsonify(rows)
            resp.status_code = 200
            return resp
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            db.close()
    @app.route('/api/sensors-statistic-0/<string:key>:"<string:field>"<string:operator>"<string:value>"')
    def sensors_statistic__filter_value_0(key, field, operator, value):
        if (key == 'filter'):
            try:
                [db, cursor] = connect_db()
                cursor.execute("SELECT * FROM sensors_statistic_0 WHERE " +  field + operator + value +"ORDER BY time")
                columns = cursor.description 
                rows = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]
                resp = jsonify(rows)
                resp.status_code = 200
                return resp
            except Exception as e:
                print(e)
            finally:
                cursor.close()
                db.close()
    @app.route('/api/sensors-statistic-0/<string:key>:"<string:field>"from"<string:start>"to"<string:end>"')
    def sensors_statistic__filter_range_0(key, field, start, end):
        if (key == 'filter'):
            try:
                [db, cursor] = connect_db()
                cursor.execute("SELECT * FROM sensors_statistic_0 WHERE " +  field + " BETWEEN " + start + " AND " + end +"ORDER BY time")
                columns = cursor.description 
                rows = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]
                resp = jsonify(rows)
                resp.status_code = 200
                return resp
            except Exception as e:
                print(e)
            finally:
                cursor.close()
                db.close()

    @app.route('/api/sensors-statistic-1')
    def sensors_statistic_1():
        try:
            [db, cursor] = connect_db()
            cursor.execute("SELECT * FROM sensors_statistic_1 ORDER BY time")
            columns = cursor.description 
            rows = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]
            resp = jsonify(rows)
            resp.status_code = 200
            return resp
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            db.close()

    @app.route('/api/sensors-statistic-1/<string:key>:"<string:field>"<string:operator>"<string:value>"')
    def sensors_statistic__filter_value_1(key, field, operator, value):
        if (key == 'filter'):
            try:
                [db, cursor] = connect_db()
                cursor.execute("SELECT * FROM sensors_statistic_1 WHERE " +  field + operator + value +"ORDER BY time")
                columns = cursor.description 
                rows = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]
                resp = jsonify(rows)
                resp.status_code = 200
                return resp
            except Exception as e:
                print(e)
            finally:
                cursor.close()
                db.close()

    @app.route('/api/sensors-statistic-1/<string:key>:"<string:field>"from"<string:start>"to"<string:end>"')
    def sensors_statistic__filter_range_1(key, field, start, end):
        if (key == 'filter'):
            try:
                [db, cursor] = connect_db()
                cursor.execute("SELECT * FROM sensors_statistic_1 WHERE " +  field + " BETWEEN " + start + " AND " + end +"ORDER BY time")
                columns = cursor.description 
                rows = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]
                resp = jsonify(rows)
                resp.status_code = 200
                return resp
            except Exception as e:
                print(e)
            finally:
                cursor.close()
                db.close()

    @app.route('/api/switch-control')
    def switch_control():
        try:
            [db, cursor] = connect_db()
            cursor.execute("SELECT * FROM switch_control ORDER BY id")
            columns = cursor.description 
            rows = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]
            resp = jsonify(rows)
            resp.status_code = 200
            return resp
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            db.close()

    @app.route('/api/switch-control/id:<string:id>')
    def switch_control_select_id(id):
        try:
            [db, cursor] = connect_db()
            cursor.execute("SELECT * FROM switch_control WHERE id=%s", id)
            columns = cursor.description 
            rows = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]
            resp = jsonify(rows)
            resp.status_code = 200
            return resp
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            db.close()

            
    @app.route('/control/switch-0/<string:switch_id>/<string:status>', methods=['POST'])
    def switch_relay(switch_id, status):
        if (switch_id[0:5] == 'relay') & ((status == 'on') | (status == 'off')):
            print ( switch_id + ' ' + status)
            if (int(switch_id[6:7]) < 4):
                mqtt.publish(topic="control/switch-0/ras-to-ard", payload="turn " + switch_id + " "  + status, qos=2, retain=False)
            elif (int(switch_id[6:7]) < 7):
                mqtt.publish(topic="control/switch-1/ras-to-ard", payload="turn " + switch_id + " "  + status, qos=2, retain=False)
        return ('',204)


    @app.errorhandler(404)
    def not_found(error=None):
        message = {
            'status': 404,
            'message': 'Not Found: ' + request.url,
        }
        resp = jsonify(message)
        resp.status_code = 404

        return resp
