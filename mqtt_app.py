from config import mqtt
from data_update_sensors import data_update_sensors_0, data_update_sensors_1
from data_update_switch import data_update_switch
from data_insert_sensors_statistic import data_insert_sensors_statistic_0, data_insert_sensors_statistic_1
from datetime import datetime
from time import time

start_0 = time()
start_1 = time()

def mqtt_app():
    @mqtt.on_connect()
    def handle_connect(client, userdata, flags, rc):
        mqtt.subscribe('control/switch-0/ard-to-ras', qos = 2)
        mqtt.subscribe('control/switch-1/ard-to-ras', qos = 2)
        mqtt.subscribe('sensors/arduino-0/ard-to-ras/now', qos = 2)
        mqtt.subscribe('sensors/arduino-1/ard-to-ras/now', qos = 2)
    
    @mqtt.on_topic('sensors/arduino-0/ard-to-ras/now')
    def handle_mytopic(client, userdata, message):
        data = message.payload.decode().split()
        data_update_sensors_0(data)
        print('Received message on topic {}: {}'
            .format(message.topic, message.payload.decode()))
        global start_0
        if time() - start_0 > 900   :
            data_insert_sensors_statistic_0(data)
            start_0 = time()

    @mqtt.on_topic('control/switch-0/ard-to-ras')
    def handle_mytopic(client, userdata, message):
        data = message.payload.decode().split()
        if data[0] == 'turn':
            data_update_switch(data)
        print('Received message on topic {}: {}'
            .format(message.topic, message.payload.decode()))

    @mqtt.on_topic('sensors/arduino-1/ard-to-ras/now')
    def handle_mytopic(client, userdata, message):
        data = message.payload.decode().split()
        data_update_sensors_1(data)
        print('Received message on topic {}: {}'
            .format(message.topic, message.payload.decode()))
        global start_1
        if time() - start_1 > 900   :
            data_insert_sensors_statistic_1(data)
            start_1 = time()
            

    @mqtt.on_topic('control/switch-1/ard-to-ras')
    def handle_mytopic(client, userdata, message):
        data = message.payload.decode().split()
        if data[0] == 'turn':
            data_update_switch(data)
        print('Received message on topic {}: {}'
            .format(message.topic, message.payload.decode()))


    # @mqtt.on_publish()
    # def handle_publish(client, userdata, mid):
    #     print('Published message with mid {}.'
    #         .format(mid))   

    @mqtt.on_subscribe()
    def handle_subscribe(client, userdata, mid, granted_qos):
        print('Subscription id {} granted with qos {}.'
        .format(mid, granted_qos))

    @mqtt.on_log()
    def handle_logging(client, userdata, level, buf):
        if level == MQTT_LOG_ERR:
            print('Error: {}'.format(buf))

