from flask_mqtt import Mqtt
from flask_cors import CORS
from flask import Flask
import psycopg2


app = Flask(__name__)

app.config['MQTT_BROKER_URL'] = 'localhost'
app.config['MQTT_BROKER_PORT'] = 1883
app.config['MQTT_USERNAME'] = 'pi'
app.config['MQTT_PASSWORD'] = '123456'
app.config['MQTT_REFRESH_TIME'] = 1.0  # refresh time in seconds
app.config['MQTT_KEEPALIVE'] = 5 # set the time interval for sending a ping to the broker to 5 seconds
app.config['MQTT_TLS_ENABLED'] = False # set TLS to disabled for testing purposes
mqtt = Mqtt(app)

CORS(app, resources=r"/*",  allow_headers='*', supports_credentials=True)

def connect_db():
    db = psycopg2.connect(
        user = "pi_user",
        password = "raspberry",
        host = "localhost",
        port = "5432",
        database = "pi_db"
    )
    cursor = db.cursor()
    return [db, cursor]