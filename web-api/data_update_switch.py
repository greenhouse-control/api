import psycopg2
from config import connect_db
def data_update_switch(data):
    [db, cursor] = connect_db()
    # Prepare SQL query to UPDATE required records
    sql = "UPDATE switch_control SET  name = '%s', status = '%s' WHERE id = '%s'" %( data[2], data[3], data[1])
    try:
        cursor.execute(sql)  # Execute the SQL command
        db.commit() # Commit your changes in the database
    except:
        db.rollback()  # Rollback in case there is any error

    # # SELECT test
    # sql = "SELECT * FROM sensors_now "
    # try:
    #     cursor.execute(sql) # Execute the SQL command
    #     results = cursor.fetchall() # Fetch all the rows in a list of lists.
    #     for row in results:
    #         print (row)  # Now print fetched result
    # except:
    #     print ("Error: unable to fetch data")

    db.close()  # disconnect from server