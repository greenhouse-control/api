from flask_app import flask_app
from mqtt_app import mqtt_app
from config import app, mqtt

mqtt_app()
flask_app()     
if __name__ == "__main__":
    app.run()
